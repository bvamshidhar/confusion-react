import * as ActionTypes from './ActionTypes';

export const Dishes = (state = {
    isLoading: true,
    errMess: null,
    dishes:[]
    }, action) => {
    switch (action.type) {
        case ActionTypes.ADD_DISHES:
            return {...state, isLoading: false, errMess: null, dishes: action.payload};
//spread operator(...state)---takes current value of the state and then whatever else we pass will be applied as modifications
//to the state.The state itself will not be mutated, instead,we take the state, we create a new object from the original
//state and then make some changes to that object and then return that object. So that's why I am returning an immutable from here.

        case ActionTypes.DISHES_LOADING:
            return {...state, isLoading: true, errMess: null, dishes: []}

        case ActionTypes.DISHES_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
            return state;
    }
};